/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

import java.util.ArrayList;

/**
 *
 * @author vagner
 */
public class Jogador {
    private int numero;
    private String nome;

    public static void main(String[] args) {
        ArrayList<Jogador> time = new ArrayList<>();    
        time.add(new Jogador(4, "David Luiz"));
        time.add(new Jogador(10, "Neymar"));
        time.add(new Jogador(12, "Júlio César"));
        for (Jogador j: time) {
            System.out.println(j);
        }
    }    
    
    public Jogador(int numero, String nome) {
        this.numero = numero;
        this.nome = nome;
    }

    @Override
    public String toString() {
        return numero + " - " + nome;
    }
}

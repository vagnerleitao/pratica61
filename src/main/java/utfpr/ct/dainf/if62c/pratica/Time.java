/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author vagner
 */
public class Time {

    private HashMap<String, Jogador> jogadores = new HashMap<>();
    private int posicao;
    private String nome;

    public Time(int posicao, String nome){
        this.posicao=posicao;
        this.nome=nome;
        //System.out.printf("Posicao = %d Nome = %s\n",this.posicao,this.nome);
    }

    public Time() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    

    public HashMap<String, Jogador> getJogadores(){
        return this.jogadores;
    }

    public void addJogador(String posicao, Jogador dados){
        this.jogadores.put(posicao, dados);
    }
    
    
}

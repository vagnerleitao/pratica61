
import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import utfpr.ct.dainf.if62c.pratica.Time;
import utfpr.ct.dainf.if62c.pratica.Jogador;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * IF62C - Fundamentos de Programação 2
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica61 {

    
    public static void main(String[] args) {
        //System.out.println("Olá, Java!");
        
        Time time1 = new Time();
        Time time2 = new Time();
        HashMap<String, Jogador> valor = new HashMap<>();
        HashMap<String, Jogador> valor2 = new HashMap<>();
          
        time1.addJogador("Goleiro ",new Jogador(12,"Felix           "));
        time1.addJogador("Atacante",new Jogador(10,"Roberto Rivelino"));
        time1.addJogador("Zagueiro",new Jogador(2, "Carlos Alberto  "));
        time1.addJogador("Meiocampo",new Jogador(6,"Toninho         "));
        
        time2.addJogador("Goleiro ",new Jogador(12,"Aranha"));
        time2.addJogador("Zagueiro",new Jogador(2,"Durval"));
        time2.addJogador("Meiocampo",new Jogador(6,"Pirlo"));
        time2.addJogador("Atacante",new Jogador(10,"Ronaldo"));
        
        valor = time1.getJogadores();
        valor2 = time2.getJogadores();
        
        Set chaves = time1.getJogadores().keySet();
        Set chaves2 = time2.getJogadores().keySet();
        System.out.println("Posição\t\tTime  1\t\t\t\tTime  2");
        
        for(Object cada: chaves)
        {
            System.out.println(cada+"\t"+valor.get(cada)+"\t\t"+valor2.get(cada));
        }
               
    }
}
